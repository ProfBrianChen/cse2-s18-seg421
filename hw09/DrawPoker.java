import java.util.Scanner; //imports Scanner
import java.util.Random; //imports Random
import java.util.Arrays; //imports Arrays
public class DrawPoker{
public static void main(String[] args) 
{ 
    int[] deckCards = new int[52]; //Array for deck of cards that has 52 members 
    for (int i = 0; i < 52; i++) { //for loop that initializes data into the array 
      deckCards[i] = i; //int i = value for that member of the array 
    }
    shufflesDeck(deckCards); //calls the method to shuffle the deck 
    int[] teamOne = new int[5]; // five cards to team one
    int[] teamTwo = new int[5]; // five cards to team two 
    for (int i = 0; i < 5; i++) {
      teamOne[i] = deckCards[2*i]; //Even numbered shuffled card
      teamTwo[i] = deckCards[2*i + 1]; //Odd numbered shuffled card
    }

    System.out.println("Team 1's Cards: " + "\t" + "Team 2's Cards: "); //prints out hands 
    for (int i : teamOne) {
      System.out.print(i + " ");
    }
  
    System.out.print("\t"); 
    System.out.print("\t"); 

    for (int i : teamTwo) {
      System.out.print(i + " ");
    }

    int teamOnePoints = 0; //determines winner 
    int teamTwoPoints = 0;
    if (findPairs(teamOne)) { //if returned true, there was a pair and add one to the team total
      teamOnePoints += 1;
    }
    if (findPairs(teamTwo)) { //if returned true, there was a pair and add one to the team total
      teamTwoPoints += 1;
    }
    if (findThrees(teamOne)) {
      teamOnePoints += 2;
    }
    if (findThrees(teamTwo) ) {
      teamTwoPoints += 2;
    }
    //Flushes
    if (findFlush(teamOne)) {
      teamOnePoints += 4;
    }

    if (findFlush(teamTwo)) {
      teamTwoPoints += 4;
    }

    //FullHouse
    if (findFullHouse(teamOne)) {
      teamOnePoints += 8;
    }

    if (findFullHouse(teamTwo)) {
      teamTwoPoints += 8;
    }
    if (teamOnePoints == teamTwoPoints) { //if points are equal, the team with the highest card wins
      if (highCard(teamOne, teamTwo) ) {
        teamOnePoints += 1;
      }
      else {
        teamTwoPoints += 1;
      }
    }
    if (teamOnePoints > teamTwoPoints) {
      System.out.println("\n");
      System.out.println("Team 1 Wins");
    }
    else {
      System.out.println("\n");
      System.out.println("Team 2 Wins");
    }
  }
public static void shufflesDeck(int[] array) 
{
    for (int i = 0; i < array.length; i++) {
      int randomNum = (int) (Math.random() * array.length);
      int place = array[i];
      array[i] = array[randomNum];
      array[randomNum] = place;
    }
  }
public static boolean findPairs(int[] array) 
{
    for (int a = 1; a < array.length; a++) {
      if (array[a] % 13 == array[a] % 13 ) {
        return true; //if there is a pair this will return true to the main method 
      }
      } 
    return false; //if there are no pairs this will return false to the main method
  }
public static boolean findThrees(int[] array) 
{
    for (int b = 1; b < array.length; b++) {
      for (int c = b + 1; c < array.length; c++) { 
        if (array[0] % 13 == array[b] % 13 && array[0] % 13 == array[c] % 13 ) { // creates nested loop to find three of a kind
          return true;
        }
      }
    } 
    return false;
  }
public static boolean findFullHouse(int[] array) 
{
    if (findThrees(array) && findPairs(array) ) { //if there is a pair and a three of a kind then there is a full house
      return true;
    }
    return false;
  }
public static boolean findFlush(int[] array) 
{
    int counter = 0;
    for (int i = 1; i < array.length; i++) {
      if (array[0] / 13 == array[i] / 13) {
        counter += 1;
      }
    }
    if (counter == 4) {
      return true;
    }
    return false;
  }
public static boolean highCard(int[] array1, int[] array2) 
{
    int highCardA = array1[0];
    for (int i = 0; i < array1.length; i++) {
      if (array1[i] > highCardA) {
        highCardA = array1[i];
      }
    }
    int highCardB = array2[0];
    for (int i = 0; i < array2.length; i++) {
      if (array2[i] > highCardB) {
        highCardB = array2[i];
      }
    }
    if (highCardA > highCardB) { //if this method returns true, team one has a higher maximum card
      return true;
    }
    return false;
  }
}

