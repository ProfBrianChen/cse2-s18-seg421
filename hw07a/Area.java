import java.util.Scanner;
public class Area { 
public static String myShape() {
  Scanner myScanner = new Scanner (System.in); 
  System.out.print("Please enter shape (cirle, rectangle, or triangle): ");
  String myShape = myScanner.next();
  boolean validInput = true; 
  while (validInput) {
      if (myShape.equals ("circle")){ //if the user inputs circle, the program will leave the loop
      break; 
    }
      else if (myShape.equals ("rectangle")){ //if the user inputs rectangle, the program will leave the loop
      break; 
    }
      else if (myShape.equals("triangle")){ //if the user inputs triangle, the program will leave the loop
      break; 
    }
    else { 
    System.out.print ("Please enter a valid shape: "); //if none of these answers are given, the loop will restart
    myShape = myScanner.next();}
  }
    return myShape;
  } 
public static double rectangleArea (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.print ("Please enter a value for length: ");
  double length = myScanner.nextDouble(); //explicitly casts value as a double 
  System.out.print ("Please enter a value for width: ");
  double width = myScanner.nextDouble();
  double areaRectangle = length * width;
  return areaRectangle;
}
  public static double triangleArea (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.print ("Please enter a value for base: ");
  double base = myScanner.nextDouble();
  System.out.print ("Please enter a value for height: ");
  double height = myScanner.nextDouble();
  double areaTriangle = ( (base * height) / 2);
  return areaTriangle;
  }
  public static double circleArea (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.print("Please enter a value for radius: ");
  double radius = myScanner.nextDouble();
  double areaCircle;
  areaCircle = (Math.PI * radius * radius);
  return areaCircle;
}
  public static void main(String[] args){
    String myShape = myShape();
    double area = 0;
      if(myShape.equals("rectangle")){
      area = rectangleArea();
      } 
      else if (myShape.equals("triangle")){
      area = triangleArea();
      } 
      else if (myShape.equals("circle")){
      area = circleArea();
      }
    System.out.println("The area of the " + myShape + " is: " + area);
  }
}