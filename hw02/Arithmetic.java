public class Arithmetic { // Beginning new class
  public static void main (String [] args) { // Beginning public static
  int numPants = 3; //Number of pairs of pants  
  double pantsPrice = 34.98; //Cost per pair of pants
  int numShirts = 2; //Number of shirts
  double shirtPrice = 24.99; //Cost per shirt
  int numBelts = 1; //Number of belts
  double beltCost = 33.99; //Cost per belt
  double paSalesTax = 0.06; //Tax rate
  double totalCostofPants = numPants * pantsPrice; //total cost of pants
  double totalCostofShirts = numShirts * shirtPrice; //total cost of shirts
  double totalCostofBelts = numBelts * beltCost; //total cost of belts
  double pantsSalesTax = ((int)((paSalesTax * totalCostofPants) *100)) / 100.0; //sales tax on pants 
  double shirtsSalesTax = ((int)((paSalesTax * totalCostofShirts) *100)) / 100.0; //sales tax on shirts 
  double beltsSalesTax = ((int)((paSalesTax * totalCostofBelts) *100)) / 100.0; //sales tac on belts 
  double totalPurchase = totalCostofPants + totalCostofShirts + totalCostofBelts; //total cost of purchases without sales tax
  double totalSalesTax = ((int)((pantsSalesTax + shirtsSalesTax + beltsSalesTax) *100)) /100.0; //total sales tax 
  double totalTransaction = totalPurchase + totalSalesTax; //total cost of transaction, including tax
  System.out.println ("The total cost for pants is $ " + totalCostofPants);
  System.out.println ("The sales tax on pants is $ " + pantsSalesTax);
  System.out.println ("The total cost for shirts is $ " + totalCostofShirts);
  System.out.println ("The sales tax on shirts is $" + shirtsSalesTax);
  System.out.println ("The total cost for belts is $" + totalCostofBelts);
  System.out.println ("The sales tax on belts is $" + totalCostofPants);
  System.out.println ("The total cost of the transaction without tax is $" + totalPurchase);
  System.out.println ("The total sales tax of the transaction is $" + totalSalesTax);
  System.out.println ("The total cost of the transaction with tax is $" + totalTransaction);
  }
}