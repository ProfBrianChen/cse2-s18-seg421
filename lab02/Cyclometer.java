public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
	int secsTrip1=480;  // Number of seconds in trip 1
  int secsTrip2=3220;  // Number of seconds in trip 2
  int countsTrip1=1561; // Number of wheel revolutions or counts in trip 1
	int countsTrip2=9037; // Number of wheel revolutions or counts in trip 2
      
 double wheelDiameter=27.0,  //diameter of wheel 
  	PI=3.14159, // value of pi
  	feetPerMile=5280, // Conversion tool of feet per mile
  	inchesPerFoot=12, // Conversion tool of inches per foot
  	secondsPerMinute=60;  // Conversion tool of seconds per minute
	double distanceTrip1, distanceTrip2,totalDistance;  
System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2; //sum of two trips
	//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");


	}  //end of main method   
} //end of class



	   