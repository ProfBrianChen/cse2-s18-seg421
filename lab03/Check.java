import java.util.Scanner; //import
public class Check { //main method required for java
  public static void main (String [] args) { //
  Scanner myScanner = new Scanner(System.in);
  System.out.print ("Enter the amount of the check in the form xx.xx: $"); //print statement
  double checkCost = myScanner.nextDouble();
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //print statement for tip
  double tipPercent = myScanner.nextDouble();
  tipPercent /= 100; //We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner: ");
  int numPeople = myScanner.nextInt();
  double totalCost;
  double costPerPerson;
  int dollars,   //whole dollar amount of cost 
  dimes, pennies; //for storing digits
  totalCost = checkCost * (1 + tipPercent);
  costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson;
  dimes=(int)(costPerPerson * 10) % 10;
  pennies=(int)(costPerPerson * 100) % 10;
  System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
  } //end of main method
} //end of class