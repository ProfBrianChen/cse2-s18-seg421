Grade: 75. 
Convert.java compiles without error, but user input of type double (the type specified in the assignment) causes runtime error. 
Calculations are correct when user input is of type int, but again, this is not what the assignment asked for.
Pyramid.java compiles and runs without error, calculations are correct.
A couple of effective comments, but somewhat minimal, especially in regards to Pyramid.java. 
Missing comment at top of program (name, date, Professor Chen, course, homework number and name). 
Output is formatted poorly. Lacking colons and/or spaces after print statements when they would be beneficial to the user.