import java.util.Scanner; //import
public class Convert { //main method required for java
  public static void main (String [] args) { //
  Scanner myScanner = new Scanner(System.in);
  System.out.print ("Enter the affected area in acres"); //print statement
  int affArea = myScanner.nextInt();
  System.out.print("Enter the rainfall in the affected area" ); //print statement 
  double rainFall = myScanner.nextDouble();
  
    int galInch = 27154; //Conversion tool, number of gallons per one inch of rainfall on one acre
    double galCubmile = 9.0817e-13; //Conversion tool, number of cubic miles in one gallon 
  
    System.out.println("The amount of rainfall in cubic miles is " +
                      (affArea * galInch * rainFall * galCubmile));
                 
  }
}
                                   
     